---
title: Freifunk User Group
logo_url: /images/services/freifunk.svg
description: Du interessiert dich für Mesh-Netzwerke? Komm vorbei!
---
## Freifunk User Group

### Wann und Wo?
* Aktuell gibt es keinen Termin in der Uni. Wir treffen uns deshalb jeden
Mittwoch in der C-Base.
* Ort: Rungestraße 20 (Nahe S+U Jannowitzbrücke)
