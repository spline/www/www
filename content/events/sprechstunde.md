---
title: Linux Sprechstunde
logo_url: /images/services/sprechstunde.png
description: Du hast Fragen zu Linux oder brauchst Hilfe bei Problemen?
---
{{< rawhtml >}}
<img src="/images/services/sprechstunde.png" style="float:
right" alt="Linux Sprechstunde" />
{{< /rawhtml >}}

# Linux Sprechstunde

Ihr habt Fragen rund um Linux oder braucht Hilfe bei Problemen? Dann kommt zur
Linux Sprechstunde von Spline!

Hier könnt ihr euch bei der Installation und Konfiguration einer
Linuxdistribution helfen lassen. Neben den üblichen WLAN oder
Bootloaderproblemen auf dem Desktop haben einige von uns auch Erfahrung mit
[OpenWRT](http://www.openwrt.org) (Linux auf Routern),
[Lineage OS](https://www.lineageos.org/) (alternative Firmware für
Androidgeräte), Shell-Skripting und BSD Derivaten.

Die Sprechstunde findet jeden ersten **Dienstag** im Monat von **16 bis 18 Uhr** im
[Splineraum](/about) (K60) statt. Wir sind auch sonst oft am Dienstag da.
Am leichtesten ist [hier](https://spaceapi.spline.de/spline) überprüfbar, ob offen ist.
