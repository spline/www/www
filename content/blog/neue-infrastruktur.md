---
title: "Spline steigt auf neue Systeme um"
date: 2024-02-01T22:12:00+01:00
draft: false
aliases:
  - /blog/2024/02/01/neue-infrastruktur/
---

Nach langer Vorbereitungszeit und vielen Problemen in den letzten Monaten ist es endlich soweit: <br><b>Spline zieht mit allen Services und Diensten auf neue Systeme um!</b>

## Was bedeutet das für euch als Nutzer?

- was passiert?
    - alle Dienste werden nach und nach von uns migriert
- wann passiert das?
    - wir ziehen Anfang Februar um
    - der Umzug wird etwa eine Woche brauchen
- was bedeutet das für mich als Nutzer?
    - alle unserer Services werden eine unbestimmte Downtime haben. Wir bemühen uns, diese Zeit so gering wie möglich zu halten, können euch aber keine festen Angaben geben.
- Muss ich als Nutzer etwas tun?
    - ihr als Nutzer müsst euch um nichts kümmern, außer der Downtime. Hier kann unsere [Statusseite](https://spline-status.de) für euch wichtig sein.
- <b>Was ist mit dem Klausurenarchiv?
    - Da auch das Klausurenarchiv Teil unserer Dienste ist, ist auch hier mit Downtime zu rechnen.
    - Wir wissen, dass derzeit die Klausurenphase ansteht und versuchen daher, dieses System so reibungslos wie möglich zu übertragen und Ausfälle zu unterbinden. </b>
- Warum macht ihr das ganze?
    - Unsere Hardware ist langsam echt in die Jahre gekommen und wir haben dadurch immer öfter Ausfälle.
    - Um weiteren größeren Ausfällen von Diensten vorzubeugen haben wir uns entschieden, endlich auf aktuellere Systeme umzurüsten.


<div class="alert alert-secondary" role="alert">
<b>Wichtig:</b> Habt ihr Dienste, die ihr wirklich dringend gerade braucht, sendet und doch bitte eine Mail an <a href="mailto:spline@spline.de">spline@spline.de</a>. Dann schauen wir, ob wir diesen Dienst schneller wieder online bringen können.
</div>
<br>
Wir hoffen, dass ihr nicht zu starke Probleme habt und wünschen euch eine erfolgreiche Klausurenphase und schöne Ferien!
<br><b>euer Spline</b>
