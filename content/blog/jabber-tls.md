---
title: "jabber.spline.de TLS updates"
tags: ['jabber', 'security']
date: 2015-07-25T02:00:00+00:00
draft: false

---

TL;DR:

Wir sind auf ejabberd Version 14.07 gewechselt und haben die TLS Einstellungen drastisch verschärft.

---

<a href="http://web.jabber.ccc.de">Jabber.ccc.de</a> et al haben vor vor ein
paar Tagen PFS TLS ciphers für server-to-server Verbindungen vorausgesetzt. Als
Konsequenz hat unser Jabber-Server (Debian oldstable) sich nicht mehr mit
diesen verbinden können. Wir haben diesen Umstand als Anlass genommen endlich
mal zu aktualisieren und haben auch gleichzeitig unsere TLS-Einstellung
angehoben. Sogar noch etwas strenger als jabber.ccc.de. Folgende Änderungen
haben wir durchgeführt:

 + ejabberd auf 14.07 (Debian Jessie) aktualisiert
 + ejabberd.cfg in das neue, viel schönere ejabberd.yml konvertiert

Folgende TLS-Einstellungen sind darin enthalten:

### s2s

    s2s_use_starttls: required_trusted
    s2s_protocol_options:
      - "no_sslv3"
      - "no_tlsv1"
    
    s2s_ciphers: "kEDH:kEECDH:-aDSS:-3DES:-DES:-RC4:-ADH:-SHA1:-AES128"

### c2s

    listen:
      -
        port: 5222
        ip: "::"
        module: ejabberd_c2s
        certfile: "/etc/ssl/private/jabber.spline.de.combined.pem"
        starttls: true
        starttls_required: true
        protocol_options:
          - "no_sslv3"
          - "no_tlsv1"
        ciphers: "kEDH:kEECDH:-aDSS:-3DES:-DES:-RC4:-ADH:-SHA1:-AES128"
        max_stanza_size: 65536
        shaper: c2s_shaper
        access: c2s

Da Port 5223 seit Jahren deprecated ist, ist er hier auch gar nicht mehr aktiviert.

Mit diesen Einstellungen erreichen wir übrigens einen perfekten Score beim [XMPP IM Observatory](https://xmpp.net/result.php?domain=jabber.spline.de&type=client).

Als Konsequenz sind jetzt aber einige, kleinere Jabber-Server nicht mehr
erreichbar. Die MaintainerInnen müssen dort auch nachlegen und zumindest ein
TLS Setup konfigurieren, dass die gelisteten PFS ciphers unterstützt. Dazu gehört:

 + TLS im Jabber Server aktivieren (ejabberd aus debian wheezy geht *nicht* ohne Patches)
 + valides Zertifikat (CAcert oder im Store von Debians ca-certificates)
 + der Common Name (CN) muss mit dem vHost des Jabber Servers übereinstimmen (*nicht* dem FQDN des Servers)
 + vermutlich SSLclient und SSLserver purpose bits im Zertifikat (StartCom unterstützt das imho *nicht* [1])


Zum Testen habe ich vorhandene XMPP-bezogene Patches[2,3] in den OpenSSL 1.0.2 branch gemerged[4]. Damit kann der eigene Server z.B. so getestet werden:


    #!/bin/sh
    
    jabberdomain=jabber.spline.de
    
    for proto in xmpp-server xmpp-client ; do
        SRV=_${proto}._tcp.$jabberdomain
        [ $proto == xmpp-client ] && SSLproto=xmpp || SSLproto=xmpp-server
        for r in  $(dig -t srv $SRV +short | sed -r 's/.* ([0-9]+) (.*)./\2:\1/g') ; do
            ./apps/openssl s_client -connect $r -starttls $SSLproto -xmpphost $jabberdomain < /dev/null
        done
    done

Bei Fragen meldet euch bitte am Besten an [spline@spline.de](mailto:spline@spline.de)


[1]
https://forum.startcom.org/viewtopic.php?f=15&t=2328

[2]
http://rt.openssl.org/Ticket/Display.html?id=3742

[3]
http://rt.openssl.org/Ticket/Display.html?id=2860

[4]
https://github.com/octomike/openssl/tree/xmpp

