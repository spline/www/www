---
title: "neue Website"
tags: ['website', 'cyrax', 'spline']
date: 2012-07-01T02:00:00+00:00
draft: false

---
Nach langen hin und her haben wir es endlich geschafft unsere alte Mediawiki zu
entsorgen und auf eine statische Seite zu wechseln.
Generiert wird diese mit Hilfe von Cyrax. Dies ist ein in Python geschriebener
statischer Webseitengenerator. Wer mehr dazu erfahren will, kann hierzu in
unsere HowTo-Sektion gucken oder bei github selbst den Code durchwühlen.
