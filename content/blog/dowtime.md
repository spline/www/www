---
title: "Downtimeankündigung für den 3.6"
tags: ['service', 'downtime']
date: 2010-06-02T02:00:00+00:00
draft: false

---
Morgen, den 3.6.10 wird es zu einer längeren Downtime kommen, da wir unsere
kompletten Server in ein anderes Rack umziehen werden.
