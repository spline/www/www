---
title: "Linux-Kurs SS 16"
tags: ['linux-kurs']
date: 2016-04-19T02:00:00+00:00
draft: false

---

Dieses Semester veranstaltet spline mal wieder den Linux-Kurs! Weitere
Informationen gibt es auf dieser Seite:

<a href="http://linuxkurs.spline.de">http://linuxkurs.spline.de</a>

Beste Grüße,<br />
spline

