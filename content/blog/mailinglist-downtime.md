---
title: "Mailinglisten nicht erreichbar!"
tags: ['service', 'mailman', 'lists']
date: 2011-12-25T02:00:00+00:00
draft: false

---
Die Mailinglisten von spline sind zur Zeit offline. Wir bedauern den Ausfall
und bitten um Entschuldigung.

*Nachtrag:* Grund war ein Festplattenausfall. Die meisten Daten konnten wieder
hergestellt werden genaueres im Splineprotokoll vom Januar.

