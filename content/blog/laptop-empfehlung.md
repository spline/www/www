---
title: "Laptop-Empfehlung für Erstis"
date: 2024-09-14T17:16:30+02:00
draft: false
aliases:
  - /laptop
  - /blog/2022/03/16/laptop-empfehlung-2022/
---

<div class="alert alert-warning" role="alert">
    <b>Hinweis</b>:
    Die Empfehlungen richten sich an Studierende, die sich für das Studium einen Laptop anschaffen möchten. Die Aspekte des Studiums, die ein Linux-System vorraussetzen können alternativ zum eigenen Linux-System auch an den institutseigenen Linux-Pool-Rechnern oder via SSH auf dem Linux-Server <a href="https://www.mi.fu-berlin.de/w/IT/ComputeServer#Linux">andorra</a> absolviert werden
</div>

Pünktlich zum Studium kaufen sich viele einen eigenen Laptop. Damit ihr nicht den Falschen kauft ein paar Tipps vorweg.

Unsere Empfehlungen sind darauf ausgerichtet, dass Linux auf dem Gerät gut funktioniert.

Das ist im Studium aus folgenden Gründen hilfreich:
* Linux wird in einigen Modulen eingesetzt.
* Linux ist in der Informatik extrem verbreitet, daher haben auch einige Tutor\*innen nur damit Erfahrung, und können sonst nicht so leicht helfen.
* Im Laufe des Studiums werden verschiedene Programmierumgebungen benötigt. Von Seiten der Uni gibt es wenig Hilfe bei der Einrichtung, und wir können nur mit Linux helfen, da wir selbst nichts anderes benutzen.

Sobald ihr ein Gerät habt, helfen wir euch gerne im Rahmen unserer Install-Party bei der Installation. Wir empfehlen [Fedora Workstation](https://www.fedoraproject.org/en/workstation/) zu installieren.

## Allgemeine Mindestanforderungen

- Arbeitsspeicher:
    - min. 8 GB besser 16 GB
- CPU:
    - mindestens 4 Kerne
- GPU: 
    - für das Studium ist keine dedizierte Grafikkarte erforderlich
- Massenspeicher: 
    - SSD mit mindestens 256 GB (keine HDD!)
- Display:
    - Das Display sollte min. FullHD (1080p) sein.
    - Entspiegelte bzw. matte Displays sind sinnvoll um auch mal auf der Wiese in der Sonne arbeiten zu können.
    - Optional: Stifteingabe, um kein separates Gerät zum Mitschreiben zu benötigen.
- Architektur:
    - 64-Bit x86 Prozessor
    - Der Prozessor sollte von AMD oder Intel stammen (nur kein Qualcomm).

<div class="alert alert-secondary" role="alert">
<p><b>Wichtig:</b> Grundsätzlich raten wir von "Consumer"- oder "Gaming"-Geräten ab.
Hersteller halten sich bei diesen Geräten oft nicht an gängige Standards, was den Linux-Support erschwert und unnötigen Aufwand verursacht.
Die meisten Business-Notebooks sind ohnehin pflegeleichter und gehen nicht so schnell kaputt.</p>

<p>Von allen Notebooks der Firmen Acer und Huawei raten wir ab. Der Linux-Support der Acer-Geräte ist generell schlecht und oft mit viel Bastelei verbunden. Auch Chromebooks sind nicht empfehlenswert.</p>

<p>Von allen Snapdragon X-Geräten raten wir ebenfalls ab. Diese haben aktuell keinen wirklichen Linux-Support und außerdem wird im 3. Semester im Modul Rechnerarchitektur ein 64-Bit x86-Prozessor benötigt.<p></div>

Oft lässt sich durch Abwählen des Betriebssystems bei der Konfiguration im Shop zusätzlich Geld sparen (Windows wird im gesamten Studium sowieso nicht benötigt).

Für den kleinen Geldbeutel empfehlen wir den Kauf eines generalüberholten ThinkPads. Hier gibt es oft zwischen 100-500€ gebrauchte Business-Geräte, die euch mit etwas Pflege gut durch das gesamte Studium begleiten können.
Anlaufstellen sind hier Seiten wie eBay Kleinanzeigen oder [Notebookshop Berlin](https://www.notebookstore.de/Refurbished-Thinkpads/).

Wenn es etwas Neues sein sollte empfielt sich ein aktuelles ThinkPad über Lenovo Campus (Serien T, X, P), ein Dell (Inspiron, Latitude, Precision, XPS), Star Labs, System76 oder Framework Laptop.

Auf der [Website des Linux Vendor Firmware Service (LVFS)](https://fwupd.org/lvfs/devices/) kann überprüft werden, ob der Hersteller eines in Frage kommenden Gerätes sich um die Linux-Unterstützung kümmert.
