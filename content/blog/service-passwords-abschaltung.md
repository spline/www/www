---
title: "Dienst-Spezifische Passwörter werden abgeschaltet / Ending support for Service Passwords"
tags: ['dienste', 'spline']
date: 2024-04-02T18:30:36+00:00
draft: false
---

Please find an english translation below.


**TL;DR**: Wenn du bei Spline Accounts für spezifische Dienste unterschiedliche Passwörter gesetzt hast, musst du ab dem 02.05.2024 wieder das Hauptpasswort deines Accounts verwenden.
Wenn du keine Unterbrechung deiner Verbindung zum Dienst haben möchtest, deaktiviere bitte vorher schon die extra-Passwörter auf [accounts.spline.de](https://accounts.spline.de/).

Da unser eigenes Plugin nicht mehr mit aktuellen Versionen von OpenLDAP funktioniert, und Service Passwörter mit unserer schrittweisen Migration auf OAuth ohnehin nicht mehr funktionieren würden, haben wir uns entschieden die Funktion einzustellen.

Bitte setze deine Dienst-Passwörter auf [accounts.spline.de](https://accounts.spline.de/) bitte vor dem 02.05.2024 zurück, oder melde dich am Stichtag mit deinem Hauptpasswort neu an.


### English

**TL;DR**: If you have set different passwords for specific services on Spline accounts, you will have to use the main password of your account again from the 02.05.2024 onwards.

If you do not want your connection to the service to be interrupted, please deactivate the extra passwords on [accounts.spline.de](https://accounts.spline.de/) before this date.

Since our custom plugin no longer works with current versions of OpenLDAP, and service passwords would no longer work with our gradual migration to OAuth anyway, we have decided to discontinue the feature.

Please reset your service passwords on [accounts.spline.de](https://accounts.spline.de/) before 02.05.2024, or log in again with your main password on the deadline.
