---
title: "Splinokalypse"
tags: ['dienste', 'spline']
date: 2013-02-15T02:00:00+00:00
draft: false

---

In den letzten 24 Stunden kam es zu einem größeren Ausfall von Spline-Diensten.
Grund hierfür war der Defekt einer Phase des Drehstroms im Serverraum.
Daraufhin musste die gesamte Server-Infrastruktur heruntergefahren werden.
Mittlerweile sollten alle Dienste wieder funktionsfähig sein.
Falls dennoch Probleme auftreten wären wir über Rückmeldungen dankbar!

