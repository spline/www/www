---
title: "Installparty SoSe 2023"
date: 2023-04-12T13:47:22+02:00
draft: false
---
Zum Start des Sommersemesters 2023 veranstalten wir in Zusammenarbeit mit dem Mentoring in den O-Wochen, genauer am Donnerstag, den 13. April 2023 (morgen) von 15-18 Uhr, wieder die Linux Install Party. Fedora frisch aus dem Ofen.
Aktuelle Referenzen findet ihr [hier](/installparty).
