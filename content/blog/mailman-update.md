---
title: "Mailman auf 2.1.11 aktualisiert"
tags: ['service', 'mailman']
date: 2008-12-01T02:00:00+00:00
draft: false

---
Die Mailinglisten-Software läuft jetzt in neuster Version 2.1.11.
