---
title: "About"
date: 2021-03-03T23:09:56+01:00
draft: false
---

## Was ist Spline?

Spline ist das Studentische Projekt Linux Netzwerke.
Die Mitglieder von spline wollen Freie Software verstehen und fördern.
Zu diesem Zweck betreiben wir ein Netz aus Arbeitsplatzrechnern und Servern,
und stellen eine Reihe von Diensten zur Verfügung. Zudem beschäftigen wir uns
kritisch mit Informatik und sind im Bereich Datenschutz aktiv.
Spline existiert seit 1998 und wurde von inzwischen mehr als 200 Menschen am
Leben gehalten.

## Für wen ist spline?

Jede und Jeden am Fachbereich Mathe & Informatik, -mit beliebig viel oder wenig 
Vorwissen- der/die

* Fragen zu GNU/Linux, freier Software, Netzwerken & Sicherheit, Datenschutz hat.
* sich für diese Themen einfach nur interessiert.
* ein Projekt mit oder bei spline machen möchte.
* Linux & Hardware kennen lernen will.
* Menschen sucht die ähnliche Fragen haben, und vielleicht auch schon einen Teil der Antworten kennen.

Für unseren Raum gilt diese [Raumpolicy](/raumpolicy), zu der sich alle Menschen, die den Raum betreten, einverstanden erklären.

## Wann sind wir zu erreichen?

Im Semester meist zu den üblichen Vorlesungzeiten: einfach vorbeikommen!
Es gibt desweiteren jeden Monat ein Splinetreffen. Hierzu sind auch ausdrücklich
Interessierte eingeladen!

## Wie erreicht Ihr uns?

* Persönlich: Raum K60 des [Instituts für Informatik](https://www.mi.fu-berlin.de/inf/) - [FU Berlin](https://www.fu-berlin.de) ([Karte](https://www.openstreetmap.org/node/7278469144)).
* Per Telefon: [(+49) 30 - 838 538 60](tel:+493083853860)

  Hinweis: Die Nummer wird nur abgenommen, wenn jemand im Raum ist (Auf der Hauptseite steht dann "offen").
* Per eMail: [spline@spline.de](mailto:spline@spline.de)

Fragen zu unserem [Mailinglistensystem](https://lists.spline.de) bitte direkt an [mailman@spline.de](mailto:mailman@spline.de) schicken.
Allgemeine, öffentliche Linux-Fragen bitte an die [öffentliche Linux-Mailingliste Splinux](https://lists.spline.de/mailman/listinfo/splinux) schicken.

## Adresse

```
Freie Universität Berlin
Institut für Informatik
spline
Takustr. 9
14195 Berlin
Berliner Stadtplan
Raum: K60, Untergeschoss (ebenerdig zu den Innenhöfen)
```
