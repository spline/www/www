---
title: Splinevorstellung auf dem Brückenkurs
---
# Splinevorstellung auf dem Brückenkurs

Dieses Howto soll splinees helfen das Projekt in der Öffentlichkeit zu
präsentieren.

## Anlässe

Gehalten wird dieser Vortrag meist, während des Blockkurses des Institut
für Informatiks. Vor dem Vortag lohnt es sich zu klären, ob spline im
Rahmen des Brückkurses auch weitere Veranstalltungen durchführen soll (
Wlan-Config, Installparty, Programmier- oder Shellkurs) und wann das
Splinetreffen ist, zu dem die Erstis eingeladen werden sollen.

## Folien

Zur Vorstellung gehört eine Präsentation geschrieben in
[landslide](https://github.com/adamzap/landslide/). Die Präsentation
selbst, wird dabei in markdonw geschreiben, der Quelltext ist unten
angehängt. Daraus entsteht dann eine Präsentation in HTML5, CSS und
JavaScript. Ein Beispiuel die Präsentation von
[2012](http://uves.spline.de/vorstellung)

Die Präsentation wird mit folgenden Command erstellt:

    landslide --embed pres.md

Der source code aus `pres.md` findet sich im letzten Abschnitt dieses
Dokuments

## Präsentations Source

    #
    <img src='./logo.svg' alt='spline logo' width='100%' align='center'>

    ---
    # Was ist spline?
    * spline = Studentisches Projekt Linux Netzwerke
    * eigentlich nur eine Gruppe von Studenten
    * gegründet ca. 1998
    * mit dem Ziel: Unterstrüzung freier Software
    * eingetragner [Hackerspace](http://hackerspaces.org/wiki/Spline)

    ---
    # Was macht spline?
    * Dienste anbieten
        * [pad.spline.de](http://pad.spline.de)
        * [dev.spline.de](http://dev.spline.de)
        * [lists.spline.de](http://lists.spline.de)
        * und [mehr](http://www.spline.de/index.php/Projekte)
    * mit freier Hardware und Software spielen
        * Server und Workstations
        * Netzwerkinfrasturktur
    * Veranstalltungen
        * monatliches Treffen
        * SOC (spline on a chip)
        * Splinetalks ([talks.spline.de](http://talks.spline.de))
        * Installpartys

    ---
    # Was könnt ihr bei Spline machen?
    * als Nutzer
        * $DIENST nutzen
        * mit Linux-/FreeBSD-/OSS-Problemen zu uns kommen
    * als Mitglied
        * Freie Software entdecken / unterstützen
        * Linux und *BSD benutzen und verwalten
        * Netzwerk administrieren
        * Lernen und Verstehen
        * Anderen helfen (siehe oben)
        * lernen wie (schlecht) man ohne Doku arbeitet

    ---
    # Was bietet euch spline?

    * 1 GBit DFN-Uplink mit /24 IPv4-Subnet
    * 10 Server
    * ein Raum (im Keller K60)
    * alten Krempel und Werkzeug
    * viele Mitstreiter
    * viel Spass

    ---
    # Wie könnt ihr mitmachen?
    Die meisten Mitglieder wissen, wie man einen Splineaccount einrichtet

    * Komm einfach vorbei, wir legen dir einen Account an oder
    * Komm zu einem der Termine

    ## Nächste Termine
    TODO: Termine raus suchen
    ---
    # Nochmal die Eckpunkte

    * Wo? Raum K60
    * Wann? Immer oder heute zum Treffen
    * Was? Alles rund um freie Software

    diese Folien: [uves.spline.de/vorstellung](uves.spline.de/vorstellung)

    Folien gebaut mit [landslide](https://github.com/n1k0/landslide)

    ---
    # Danke und Tschüss