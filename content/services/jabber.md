---
title: Jabber
logo_url: /images/services/jabber.svg
description: Spline-Server im dezentralen Jabber-Netz
---

## Spline Jabber

Spline hat einen eigenen Jabber-Server, erreichbar unter
**jabber.spline.de**.

---

Jabber ist ein XML-basiertes Protokoll, dass ursprünglich als Chatsystem
entwickelt wurde und darin auch bis heute eine Hauptanwendung findet. Der
Standard wurde inzwischen in XMPP umgetauft, aber der Begriff "Jabber" ist,
nicht zuletzt wegen der vielen Implementierungen, die die Bezeichnung im Namen
haben, nicht mehr wegzudenken.

### Features

Chats mit XMPP haben unter anderem folgende Vorteile:

+ **sicherer** Nachrichtentransport (für Ende-zu-Ende Verschlüsselung muss aber
z.B. [OTR](https://otr.cypherpunks.ca/) über XMPP benutzt werden)
+ TLS 1.2 mit Perfect Forward Secrecy
+ **Dezentralität**
  - jede Benutzerin eines XMPP-Servers kann mit Benutzern anderer
XMPP-Server kommunizieren
  - Trust your local hack space!
+ Multi-User Chat
+ Prioritäten und multiple Präsenzen für mehrere Geräte
+ leichte Erweiterbarkeit
+ offener Standard

### Benutzung

Die Registrierung über das Jabber-Protokoll ist nicht möglich, jabber.spline.de
ist stattdessen an [accounts.spline.de](https://accounts.spline.de/) angeschlossen.
Ihr könnt euch dort einfach einen Account erstellen und gegebenenfalls ein
separates Passwort verwenden. Die Jabber-ID ist dann
**benutzername@jabber.spline.de**

Mit der Jabber-ID und eurem Passwort könnt ihr euch dann mit vielen Clients
verbinden. Getestet haben wir unter anderem

+ Pidgin + Adium (libpurple) (Linux/OSX)
+ Gajim (Linux)
+ Empathy (Linux)
+ Conversations (Android)
+ iChat (OSX)

Manche älteren Clients lösen den DNS-Namen nicht korrekt auf. Falls ihr
Verbindungsprobleme habt, könnt ihr die Hostnames vm-jabber0. oder
vm-jabber1.spline.de als Server benutzen.

Auch wenn der Sicherheitsgewinn marginal ist, findet ihr hier den
SSL-Fingerprint, falls ihr der Zertifizierungsstelle unseres Zertifikats nicht
vertraut:

    (SHA-1)0D:A9:A4:8C:8D:2F:9B:26:6B:0F:30:AC:F7:E6:13:93:CE:9C:E7:14
    (SHA-256)7D:22:13:EE:52:84:7E:F4:DA:ED:6C:15:DA:29:BB:BB:1B:20:0B:F7:B4:5C:64:F8:CD:5E:79:EA:06:79:2C:AF
    (SHA-512)5D:BB:53:80:2B:80:39:BF:5F:5D:6E:5F:30:E7:AB:2B:B5:4D:B8:BB:46:3C:F5:8C:CE:5C:B3:49:AF:07:CA:22:B6:A4:9C:00:D1:53:5F:F2:25:61:56:91:10:E1:5C:2A:0D:8A:82:AF:F8:33:54:EA:AE:9E:CB:BB:F9:C5:C7:D2

Wenn ihr Fragen habt oder Bugs findet, könnt ihr euch jederzeit gerne an uns
wenden: **spline@spline.de**


#### The Dark Side

Der Name Jabber wird auch von der Firma Cisco benutzt, die "Jabber, Inc" im Jahr
2008 erworben hat. Auch wenn Cisco Instant Messaging Systeme vertreibt ist mit
einer Kompatibilität zu XMPP eher nicht zu rechnen.

Bis vor Kurzem waren auch die Chat-Dienste der Unternehmen Google, Facebook und
United Internet (web.de, gmx.de und 1&1) über XMPP Federation direkt von allen
Jabber-Servern erreichbar. Die offene Politik wurde leider geändert, sodass die
Nutzerbasis der genannten Unternehmen leider nicht mehr Teil des XMPP-Netzwerks
ist.

---

Aktuelle Informationen zu Änderungen an unserem Jabber-Server findet ihr [hier](/tags/jabber)

