---
title: XABSL
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-11-17T12:00:00+01:00
---
XABSL, The Extensible Agent Behavior Specification Language, ist eine
Scriptsprache zur Beschreibung und Steuerung von Verhaltensabläufen autonomer
Systeme. Dieser Vortrag gibt eine Einführung in XABSL und die Steuerung von
[FUmanoiden](http://fumanoids.de/) Robotern.

## Vortragende
Rico Jonschkowski

## Links
* [XABSL](http://www.robocup.informatik.tu-darmstadt.de/xabsl/)