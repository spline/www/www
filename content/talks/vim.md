---
title: Real Programmers use Vim
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2011-04-20T12:00:00+01:00
---
Vim - ein sehr mächtiger Text-Editor und warum Du ihn benutzen solltest.

Von den Unterschieden zwischen Vim und anderen Editoren über die Konfiguration
mittels .vimrc bis zu diversen netten Scripten und Plugins soll euch dieser
Vortrag zeigen, was mit Vim alles möglich ist, warum der Editor nicht nur ein
Programmierwerkzeug ist und es sich lohnt, Vim Ninja zu werden.

## Vortragende
Stefan Otte

## Slides
[vim.tar.gz](/talks/vim.tar.gz)

## Links
* [Vim](http://vim.org/)
* [https://github.com/sotte/vimrc](https://github.com/sotte/vimrc)