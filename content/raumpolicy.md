---
title: "Raumpolicy"
date: 2023-10-13T1209:56+02:00
draft: false
---

# Unsere Raum-Policy:
#### Beim Treffen vom 3. September 2013 wurde folgendes beschlossen:

Spline-Raum-Policy
Spline sieht sich in der Aufgabe, allen Studierenden gleichermaßen Zugang zu freier Software und Hardware zu verschaffen. Aus diesem Grund kann diskriminierendes und ausschließendes Verhalten nicht toleriert werden. Hierzu zählen unter anderem:

* faschistische, rassistische, antisemitische oder nationalistische Aussagen
* Sexismus, Lookismus und Ableismus in jeglicher Form
* Homo-, Trans- bzw. Xenophobie

Bei Vergehen gegen diese Regel sind Splinemitglieder befugt, die entsprechende Person zu verwarnen und notfalls für 24 Stunden des Raumes zu verweisen. Wenn kein Lerneffekt bei der Person zu erkennen ist, kann das Splinetreffen mit einfacher Mehrheit ein allgemeines Raumverbot aussprechen.
Zur Veranschaulichung können auch folgende Motive verwendet werden: http://fsi.spline.de/wiki/index.php/Anti-Diskriminatorische_Motive

#### Zusatz vom 4. Juni 2019:
Be excellent to each other and the inventory!

## Raum-Policy Extended
TL;DR: MACH SAUBER!!!!111!!1!elf

1. Solange eine Person an einem Rechner eingeloggt ist, darf Diese Gegenstände (Unterlagen, nichtbrennbare Flüssigkeiten, etc.) am Arbeitsplatz ablegen.
1. Generell ist jede im Raum befindliche Person dazu angehalten zu jedem Zeitpunkt nach Benutzung der Arbeitsplätze sämtliche Gegenstände umgehend zu entfernen/mitzunehmen und für ein gepflegtes und aufgeräumtes Arbeitsumfeld zu sorgen.
1. Nahrungsaufnahme ist per se im Spline erlaubt. Jeder ist dazu verpflichtet nach dem Essen seinen Platz mit Reinigungsmitteln ordnungsgemäß zu säubern.
1. Sollte der letzte Schließberechtigte den Raum am Ende des Tages verlassen, ist dieser (und alle sich noch im Raum befindlichen „freiwilligen Helfer“) dazu verpflichtet, den Raum in einem ordnungsgemäßen Zustand zu verlassen und alle auf dem Tisch liegenden Gegenstände an ihren angestammten Platz, oder in die Clean-Desk-Kiste zu befördern.
1. Der Inhalt der Clean-Desk-Kiste 2 wird am Ende der Woche (≍Freitag) entsorgt oder geht in Spline-Besitz über! **DANACH** wird der Inhalt der Clean-Desk-Kiste 1 in die Clean-Desk-Kiste 2 überführt.
1. Aktivitäten im Raum sollen keinen negativen Eindruck auf Außenstehende haben.
    1. Dies betrifft unter anderem das Konsumieren von Videospielen
1. Personen im Raum haben ein Anrecht darauf, ungestört an Spline- oder Uniprojekten (Spline > Uni) zu arbeiten.

